# cloudflare_ddns

Ansible role for configuring a Cloudflare dynamic DNS updating script.

## Requirements

* A Cloudflare account is required and a domain already setup in your Cloudflare account.
* _(optional)_ [`jq`](https://stedolan.github.io/jq/) - to enable return status verification from Cloudflare API call
* _(optional)_ `dnsutils` - for faster IP lookups

## Role Variables

| Parameter | Default | Comments |
|-----------------------------------|--------------------|----------------------------------------------------------------------------------------------------------|
| `cloudflare_ddns_script_location` | /usr/local/bin | Path to install cloudflare_ddns.sh shell script |
| `cloudflare_ddns_timer_min` | 2 | Minutes between attempts to update DNS entry in Cloudflare |
| `cloudflare_ddns_dns_zone` |  | Zone ID from: https://www.cloudflare.com/a/overview/ |
| `cloudflare_ddns_auth_email` |  | Cloudflare account email |
| `cloudflare_ddns_auth_key` |  | Global API Key found under Cloudflare account settings - https://www.cloudflare.com/a/account/my-account |
| `cloudflare_ddns_domains` |  | <sup>1</sup> List of domains and identifiers, see syntax below |
| `cloudflare_ddns_prev_ip_file` | /tmp/public-ip.txt | File to store previous ip address |

<sup>1</sup> `cloudflare_ddns_domains` syntax:

```
cloudflare_ddns_domains:
  - domain: subdomain.domain.com
    id: domain_identifier
    proxied: true|false
  - domain: subdomain2.domain.com
    id: domain2_identifier
    proxied: true|false
```
`proxied` is optional and defaults to false.

To get the `id` value use this curl request:

    curl "https://api.cloudflare.com/client/v4/zones/<cloudflare_ddns_dns_zone>/dns_records?name=<cloudflare_ddns_domain>" \
        --silent -X GET \
        -H "X-Auth-Email: <cloudflare_ddns_auth_email>" \
        -H "X-Auth-Key: <cloudflare_ddns_auth_key>" \
        -H "Content-Type: application/json"

For more information see: https://api.cloudflare.com/#dns-records-for-a-zone-list-dns-records

## Dependencies

None.

## Example Playbook

    - name: Setup Cloudflare DDNS
      import_role:
        name: cloudflare_ddns

## License

This 'cloudflare_ddns' ansible role is licensed under the GPLv3 terms as it contains modifications of GPLv3 software.

    cloudflare_ddns
    Copyright (C) 2018  Matthew C. Veno

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


## Author Information

Matthew C. Veno

Inspired from Rohan Jain's post - https://www.rohanjain.in/cloudflare-ddns/
